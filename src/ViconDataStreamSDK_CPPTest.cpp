///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) OMG Plc 2009.
// All rights reserved.  This software is protected by copyright
// law and international treaties.  No part of this software / document
// may be reproduced or distributed in any form or by any means,
// whether transiently or incidentally to some other use of this software,
// without the written permission of the copyright owner.
//
///////////////////////////////////////////////////////////////////////////////

#include "Client.h"

#include <iostream>
#include <fstream>
#include <cassert>
#include <ctime>
#include <iomanip>

#ifdef WIN32
#include <conio.h>   // For _kbhit()
#include <cstdio>   // For getchar()
#include <windows.h> // For Sleep()
#endif // WIN32

#include <time.h>

using namespace ViconDataStreamSDK::CPP;

#define output_stream if(!LogFile.empty()) ; else std::cout

#define FLOAT_PRECISION 5

/*
 * Recorded file format
 *
 *  Each line contains data of a single frame. Each line is build up as follows
 *
 *  - (unsigned int) FrameNumber
 *  - (unsigned int) Output_GetTimecode.Hours;
 *  - (unsigned int) Output_GetTimecode.Minutes;
 *  - (unsigned int) Output_GetTimecode.Seconds;
 *  - (unsigned int) Output_GetTimecode.Frames;
 *  - (unsigned int) Output_GetTimecode.SubFrame;
 *  - (TimecodeStandard::Enum) Output_GetTimecode.Standard;
 *  - (unsigned int) Output_GetTimecode.SubFramesPerFrame;
 *  - (unsigned int) Output_GetTimecode.UserBits;
 *
 *  - (double) Output_GetSegmentGlobalTranslation.Translation[ 0 ]
 *  - (double) Output_GetSegmentGlobalTranslation.Translation[ 1 ]
 *  - (double) Output_GetSegmentGlobalTranslation.Translation[ 2 ]
 *
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 0 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 1 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 2 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 3 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 4 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 5 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 6 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 7 ]
 *  - (double) Output_GetSegmentGlobalRotationMatrix.Rotation[ 8 ]
 *
 *  - (double) Output_GetSegmentGlobalRotationQuaternion.Rotation[ 0 ]
 *  - (double) Output_GetSegmentGlobalRotationQuaternion.Rotation[ 1 ]
 *  - (double) Output_GetSegmentGlobalRotationQuaternion.Rotation[ 2 ]
 *  - (double) Output_GetSegmentGlobalRotationQuaternion.Rotation[ 3 ]
 *
 *  - (double) Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 0 ]
 *  - (double) Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 1 ]
 *  - (double) Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 2 ]
 *  "\n"
 */


namespace
{
std::ofstream& begin_record(std::ofstream& ofs)
{
    static bool first_record = true;

    if (!first_record)
    {
        ofs << "\n";
    }

    if (first_record)
    {
       first_record = false;
    }

    ofs << std::setprecision(FLOAT_PRECISION);
    return ofs;
}

std::ofstream& record(std::ofstream& ofs)
{
    ofs << " ";
    ofs << std::setprecision(FLOAT_PRECISION);
    return ofs;
}

std::ofstream& end_record(std::ofstream& ofs)
{
    return record(ofs);
}

std::string Adapt( const bool i_Value )
{
    return i_Value ? "True" : "False";
}

std::string Adapt( const Direction::Enum i_Direction )
{
    switch( i_Direction )
    {
    case Direction::Forward:
        return "Forward";
    case Direction::Backward:
        return "Backward";
    case Direction::Left:
        return "Left";
    case Direction::Right:
        return "Right";
    case Direction::Up:
        return "Up";
    case Direction::Down:
        return "Down";
    default:
        return "Unknown";
    }
}

std::string Adapt( const DeviceType::Enum i_DeviceType )
{
    switch( i_DeviceType )
    {
    case DeviceType::ForcePlate:
        return "ForcePlate";
    case DeviceType::Unknown:
    default:
        return "Unknown";
    }
}

std::string Adapt( const Unit::Enum i_Unit )
{
    switch( i_Unit )
    {
    case Unit::Meter:
        return "Meter";
    case Unit::Volt:
        return "Volt";
    case Unit::NewtonMeter:
        return "NewtonMeter";
    case Unit::Newton:
        return "Newton";
    case Unit::Kilogram:
        return "Kilogram";
    case Unit::Second:
        return "Second";
    case Unit::Ampere:
        return "Ampere";
    case Unit::Kelvin:
        return "Kelvin";
    case Unit::Mole:
        return "Mole";
    case Unit::Candela:
        return "Candela";
    case Unit::Radian:
        return "Radian";
    case Unit::Steradian:
        return "Steradian";
    case Unit::MeterSquared:
        return "MeterSquared";
    case Unit::MeterCubed:
        return "MeterCubed";
    case Unit::MeterPerSecond:
        return "MeterPerSecond";
    case Unit::MeterPerSecondSquared:
        return "MeterPerSecondSquared";
    case Unit::RadianPerSecond:
        return "RadianPerSecond";
    case Unit::RadianPerSecondSquared:
        return "RadianPerSecondSquared";
    case Unit::Hertz:
        return "Hertz";
    case Unit::Joule:
        return "Joule";
    case Unit::Watt:
        return "Watt";
    case Unit::Pascal:
        return "Pascal";
    case Unit::Lumen:
        return "Lumen";
    case Unit::Lux:
        return "Lux";
    case Unit::Coulomb:
        return "Coulomb";
    case Unit::Ohm:
        return "Ohm";
    case Unit::Farad:
        return "Farad";
    case Unit::Weber:
        return "Weber";
    case Unit::Tesla:
        return "Tesla";
    case Unit::Henry:
        return "Henry";
    case Unit::Siemens:
        return "Siemens";
    case Unit::Becquerel:
        return "Becquerel";
    case Unit::Gray:
        return "Gray";
    case Unit::Sievert:
        return "Sievert";
    case Unit::Katal:
        return "Katal";

    case Unit::Unknown:
    default:
        return "Unknown";
    }
}
#ifdef WIN32
bool Hit()
{
    bool hit = false;
    while( _kbhit() )
    {
        getchar();
        hit = true;
    }
    return hit;
}
#endif
}

int main( int argc, char* argv[] )
{
    // Program options

    std::string HostName = "localhost:801";
    if( argc > 1 )
    {
        HostName = argv[1];
    }
    // log contains:
    // version number
    // log of framerate over time
    // --multicast
    // kill off internal app
    std::string LogFile = "";
    std::string MulticastAddress = "244.0.0.0:44801";
    std::string object;
    bool ConnectToMultiCast = false;
    bool EnableMultiCast = false;
    for(int a=2; a < argc; ++a)
    {
        std::string arg = argv[a];
        if(arg == "--help")
        {
            std::cout << argv[0] << " <HostName>: allowed options include:\n  --record_file <LogFile> --enable_multicast <MulticastAddress:Port> --connect_to_multicast <MulticastAddress:Port> --help" << std::endl;
            return 0;
        }
        else if (arg=="--record_file")
        {
            if(a < argc)
            {
                LogFile = argv[a+1];
                std::cout << "Recording to <"<< LogFile << "> ..." << std::endl;
                ++a;
            }
        }
        else if (arg=="--enable_multicast")
        {
            EnableMultiCast = true;
            if(a < argc)
            {
                MulticastAddress = argv[a+1];
                std::cout << "Enabling multicast address <"<< MulticastAddress << "> ..." << std::endl;
                ++a;
            }
        }
        else if (arg=="--connect_to_multicast")
        {
            ConnectToMultiCast = true;
            if(a < argc)
            {
                MulticastAddress = argv[a+1];
                std::cout << "connecting to multicast address <"<< MulticastAddress << "> ..." << std::endl;
                ++a;
            }
        }
        else if(arg=="--object")
        {
            if(a < argc)
            {
                object = argv[a+1];
                std::cout << "Recorded object: <"<<  object << "> ..." << std::endl;
                ++a;
            }
        }
        else
        {
            std::cout << "Failed to understand argument <" << argv[a] << ">...exiting" << std::endl;
            return 1;
        }
    }

     if (LogFile.empty())
     {
         std::cout << "Please specify a recoding file via option --record_file" << std::endl;
         return -1;
     }

    std::ofstream ofs;
    if(!LogFile.empty())
    {
        ofs.open(LogFile.c_str());
        if(!ofs.is_open())
        {
            std::cout << "Could not open log file <" << LogFile << ">...exiting" << std::endl;
            return 1;
        }
    }
    // Make a new client
    Client MyClient;

    for(int i=0; i != 3; ++i) // repeat to check disconnecting doesn't wreck next connect
    {
        // Connect to a server
        std::cout << "Connecting to " << HostName << " ..." << std::flush;
        while( !MyClient.IsConnected().Connected )
        {
            // Direct connection

            bool ok = false;
            if(ConnectToMultiCast)
            {
                // Multicast connection
                ok = ( MyClient.ConnectToMulticast( HostName, MulticastAddress ).Result == Result::Success );

            }
            else
            {
                ok =( MyClient.Connect( HostName ).Result == Result::Success );
            }
            if(!ok)
            {
                std::cout << "Warning - connect failed..." << std::endl;
            }


            std::cout << ".";
#ifdef WIN32
            Sleep( 200 );
#else
            sleep(1);
#endif
        }
        std::cout << std::endl;

        // Enable some different data types
        MyClient.EnableSegmentData();
        MyClient.EnableMarkerData();
        MyClient.EnableUnlabeledMarkerData();
        MyClient.EnableDeviceData();

        std::cout << "Segment Data Enabled: "          << Adapt( MyClient.IsSegmentDataEnabled().Enabled )         << std::endl;
        std::cout << "Marker Data Enabled: "           << Adapt( MyClient.IsMarkerDataEnabled().Enabled )          << std::endl;
        std::cout << "Unlabeled Marker Data Enabled: " << Adapt( MyClient.IsUnlabeledMarkerDataEnabled().Enabled ) << std::endl;
        std::cout << "Device Data Enabled: "           << Adapt( MyClient.IsDeviceDataEnabled().Enabled )          << std::endl;

        // Set the streaming mode
        //MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPull );
        // MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPullPreFetch );
        MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ServerPush );

        // Set the global up axis
        MyClient.SetAxisMapping( Direction::Forward,
                                 Direction::Left,
                                 Direction::Up ); // Z-up
        // MyClient.SetGlobalUpAxis( Direction::Forward,
        //                           Direction::Up,
        //                           Direction::Right ); // Y-up

        Output_GetAxisMapping _Output_GetAxisMapping = MyClient.GetAxisMapping();
        std::cout << "Axis Mapping: X-" << Adapt( _Output_GetAxisMapping.XAxis )
                  << " Y-" << Adapt( _Output_GetAxisMapping.YAxis )
                  << " Z-" << Adapt( _Output_GetAxisMapping.ZAxis ) << std::endl;

        // Discover the version number
        Output_GetVersion _Output_GetVersion = MyClient.GetVersion();
        std::cout << "Version: " << _Output_GetVersion.Major << "."
                  << _Output_GetVersion.Minor << "."
                  << _Output_GetVersion.Point << std::endl;

        if( EnableMultiCast )
        {
            assert( MyClient.IsConnected().Connected );
            MyClient.StartTransmittingMulticast( HostName, MulticastAddress );
        }

        size_t FrameRateWindow = 1000; // frames
        size_t Counter = 0;
        clock_t LastTime = clock();
        // Loop until a key is pressed
#ifdef WIN32
        while( !Hit() )
#else
        while( true)
#endif
        {
            // Get a frame
            while( MyClient.GetFrame().Result != Result::Success )
            {
                // Sleep a little so that we don't lumber the CPU with a busy poll
#ifdef WIN32
                Sleep( 200 );
#else
                sleep(1);
#endif
            }
            if(++Counter == FrameRateWindow)
            {
                clock_t Now = clock();
                double FrameRate = (double)(FrameRateWindow * CLOCKS_PER_SEC) / (double)(Now - LastTime);
                if(!LogFile.empty())
                {
                    time_t rawtime;
                    struct tm * timeinfo;
                    time ( &rawtime );
                    timeinfo = localtime ( &rawtime );

                    std::cout << "Frame rate = " << FrameRate << " at " <<  asctime (timeinfo)<< std::endl;
                }

                LastTime = Now;
                Counter = 0;
            }

            // Get the frame number
            Output_GetFrameNumber _Output_GetFrameNumber = MyClient.GetFrameNumber();
            begin_record(ofs) << _Output_GetFrameNumber.FrameNumber;

            // Get the timecode
            Output_GetTimecode _Output_GetTimecode  = MyClient.GetTimecode();

            record(ofs) << _Output_GetTimecode.Hours;
            record(ofs) << _Output_GetTimecode.Minutes;
            record(ofs) << _Output_GetTimecode.Seconds;
            record(ofs) << _Output_GetTimecode.Frames;
            record(ofs) << _Output_GetTimecode.SubFrame;
            record(ofs) << _Output_GetTimecode.Standard;
            record(ofs) << _Output_GetTimecode.SubFramesPerFrame;
            record(ofs) << _Output_GetTimecode.UserBits;

            // Get the latency
            record(ofs) << MyClient.GetLatencyTotal().Total;

            /* what is a latency sample?
            for( unsigned int LatencySampleIndex = 0 ; LatencySampleIndex < MyClient.GetLatencySampleCount().Count ; ++LatencySampleIndex )
            {
                std::string SampleName  = MyClient.GetLatencySampleName( LatencySampleIndex ).Name;
                double      SampleValue = MyClient.GetLatencySampleValue( SampleName ).Value;

                output_stream << "  " << SampleName << " " << SampleValue << "s" << std::endl;
            }
            output_stream << std::endl;
            */

            // Count the number of subjects
            bool objectExists = false;
            unsigned int SubjectCount = MyClient.GetSubjectCount().SubjectCount;
            for( unsigned int SubjectIndex = 0 ; SubjectIndex < SubjectCount ; ++SubjectIndex )
            {
                // Get the subject name
                std::string SubjectName = MyClient.GetSubjectName( SubjectIndex ).SubjectName;
                if (SubjectName.compare(object) != 0)
                {
                    continue;
                }

                // Count the number of segments
                unsigned int SegmentCount = MyClient.GetSegmentCount( SubjectName ).SegmentCount;
                for( unsigned int SegmentIndex = 0 ; SegmentIndex < SegmentCount ; ++SegmentIndex )
                {
                    // Get the segment name
                    std::string SegmentName = MyClient.GetSegmentName( SubjectName, SegmentIndex ).SegmentName;
                    if (SegmentName.compare(object) != 0)
                    {
                        continue;
                    }
                    objectExists = true;

                    // Get the global segment translation
                    Output_GetSegmentGlobalTranslation _Output_GetSegmentGlobalTranslation =
                            MyClient.GetSegmentGlobalTranslation( SubjectName, SegmentName );
                    record(ofs) << _Output_GetSegmentGlobalTranslation.Translation[ 0 ];
                    record(ofs) << _Output_GetSegmentGlobalTranslation.Translation[ 1 ];
                    record(ofs) << _Output_GetSegmentGlobalTranslation.Translation[ 2 ];

                    // Get the global segment rotation as a matrix
                    Output_GetSegmentGlobalRotationMatrix _Output_GetSegmentGlobalRotationMatrix =
                            MyClient.GetSegmentGlobalRotationMatrix( SubjectName, SegmentName );
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 0 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 1 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 2 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 3 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 4 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 5 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 6 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 7 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationMatrix.Rotation[ 8 ];

                    // Get the global segment rotation in quaternion co-ordinates
                    Output_GetSegmentGlobalRotationQuaternion _Output_GetSegmentGlobalRotationQuaternion =
                            MyClient.GetSegmentGlobalRotationQuaternion( SubjectName, SegmentName );
                    record(ofs) << _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 0 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 1 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 2 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 3 ];

                    // Get the global segment rotation in EulerXYZ co-ordinates
                    Output_GetSegmentGlobalRotationEulerXYZ _Output_GetSegmentGlobalRotationEulerXYZ =
                            MyClient.GetSegmentGlobalRotationEulerXYZ( SubjectName, SegmentName );
                    record(ofs) << _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 0 ];
                    record(ofs) << _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 1 ];
                    end_record(ofs) << _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 2 ];

                    break;
                }
            }
            if (!objectExists)
            {
                std::cout << ">> Error: Object <" << object << "> does not exist." << std::endl;
                break;
            }
        }

        ofs.close();

        if( EnableMultiCast )
        {
            MyClient.StopTransmittingMulticast();
        }
        MyClient.DisableSegmentData();
        MyClient.DisableMarkerData();
        MyClient.DisableUnlabeledMarkerData();
        MyClient.DisableDeviceData();

        // Disconnect and dispose
        int t = clock();
        std::cout << " Disconnecting..." << std::endl;
        MyClient.Disconnect();
        int dt = clock() - t;
        double secs = (double) (dt)/(double)CLOCKS_PER_SEC;
        std::cout << " Disconnect time = " << secs << " secs" << std::endl;
    }
}
